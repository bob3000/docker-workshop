# Docker Workshop

This tutorial aims to explain the usage of Docker / docker-compose with a
hands on approach. You will write a `Dockerfile` as well as a
`docker-compose.yml` file to run your stack and make it work all together.

## The stack

We are going to set up the [ghost](https://ghost.org/) blogging software
backed by a MySQL database and with Nginx as reverse proxy.

## Goals

### First goal

Make the stack run and write a blog post.

The stack should come up with just one single command which is

```sh
docker-compose up -d
```

The application will be available through Nginx on
[localhost:8080](http://localhost:8080).

Go to [localhost:8080/ghost](http://localhost:8080/ghost) and set up
the set up your admin credentials. When this is done write a random
blog post and publish it. You can look at it when you just return to
[localhost:8080](http://localhost:8080). It should be the first post
in the first row of posts.

### Second goal

Make the database persistent.

After you typed

```sh
docker-compose down
docker-compose up -d
```

and go to [localhost:8080](http://localhost:8080) you will notice that
your blog post is gone. This happened because the actual MySQL database
file was stored inside the container and all the data inside the container
is ephemeral.

What you need is a [persistent volume](https://docs.docker.com/storage/volumes/).
Store the MySQL data directory in a way that it's not deleted even when the
MySQL container is replaced.

Remember the [First goal](#first-goal) - the stack should come up
with a single `docker-compose up -d` and should require no extra steps
to set up the persistent volume.

### Third goal

Take a database backup.

Make the MySQL database available to your host machine and take a database dump
using your preferred MySQL client (e.g. SequelPro, mysql command line, etc.).

## How to begin

This repository contains several files with fragments of code which have
to be completed.

Start by looking at the file [docker-compose.yml](docker-compose.yml) and
try to define the stack. To get a better understanding of the docker
compose file take a look at the [Documentation](https://docs.docker.com/compose/)

You will notice that the images defined for ghost and MySQL have no prefix
(*e.g. `workshop/<imagename>`*) and therefore are coming directly from the
[hub.docker.com](https://hub.docker.com) library. They just have
to be configured in order to work together.

The Nginx image will need a little tweaking in order to add the proper
configuration. Therefore there is a [Dockerfile](nginx/Dockerfile) which needs
to be implemented. Include the [proper configuration file](nginx/files/ghost.conf)
into the image and this way make Nginx pass the traffic through to the ghost
container.

When the Dockerfile is implemented and the Nginx configuration is
properly adjusted the docker image can be built with the `docker-compose build`
command to complete the stack.

## I still don't know how to begin

Try to get the stack running step by step. First try to run the containers
in some way by integrating them in the [docker-compose.yml](docker-compose.yml)
file. You can type `docker-compose ps` to check out what's running.
(*Note: the ghost container might crash if it can't connect to a database*).

When this is done try to connect the containers. First connect ghost container
with the database and then build the Nginx container and connect it to the
ghost container. It's not working and you're not sure why? Check out the logs
with `docker-compose logs`. The logs don't help and you need to take a look into
the container? Try `docker-compose exec nginx bash`.

## I tried implementing the stack and became frustrated

You need to fail in order to learn. Be strong - try again.
